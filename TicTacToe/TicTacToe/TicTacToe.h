#pragma once
#include <iostream>
#include <string>

class TicTacToe
{
private:
	char m_board[9] = { ' ',' ',' ',' ',' ',' ',' ',' ',' ' };
	int m_numTurns = 0;
	char m_playerTurn = '1';
	char m_winner = ' ';


public:

	TicTacToe()
	{
		;
	}

	void DisplayBoard()
	{
		std::cout << m_board[0] << "|" << m_board[1] << "|" << m_board[2] << "\n"
			<< m_board[3] << "|" << m_board[4] << "|" << m_board[5] << "\n"
			<< m_board[6] << "|" << m_board[7] << "|" << m_board[8] << "\n";
	}

	bool IsOver()
	{
		
		//This sequence checks for all the types of wins and assigns a winner if found
		if (m_board[0] == m_board[3] && m_board[6] == m_board[0])
		{
			if (m_board[0] == ' ') return false;
			else if (m_board[0] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[1] == m_board[4]&& m_board[7] == m_board[1])
		{
			if (m_board[1] == ' ') return false;
			else if (m_board[1] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[2] == m_board[5] && m_board[8] == m_board[2])
		{
			if (m_board[2] == ' ') return false;
			else if (m_board[2] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[0] == m_board[1] && m_board[2] == m_board[0])
		{
			if (m_board[0] == ' ') return false;
			else if (m_board[0] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[3] == m_board[4] && m_board[5] == m_board[3])
		{
			if (m_board[3] == ' ') return false;
			else if (m_board[3] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[6] == m_board[7] && m_board[8] == m_board[6])
		{
			if (m_board[6] == ' ') return false;
			else if (m_board[6] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[0] == m_board[4] && m_board[8] == m_board[0])
		{
			if (m_board[0] == ' ') return false;
			else if (m_board[0] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		else if (m_board[2] == m_board[4] && m_board[6] == m_board[2])
		{
			if (m_board[2] == ' ') return false;
			else if (m_board[2] == 'X')
			{
				m_winner = '1';
				return true;
			}
			else
			{
				m_winner = '2';
				return true;
			}
		}
		//Check if the board is full
		else if (m_numTurns > 8) return true;
		else return false;
	}

	char GetPlayerTurn()
	{
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		position--;
		if (m_board[position] == 'X' || m_board[position] == 'O') return false;
		else return true;
	}

	void Move(int position)
	{
		position--;
		if (m_playerTurn == '1')
		{
			m_board[position] = 'X';
			m_playerTurn = '2';
			m_numTurns++;
		}
		else
		{
			m_board[position] = 'O';
			m_playerTurn = '1';
			m_numTurns++;
		}
	}

	void DisplayResult()
	{
		if (m_winner == ' ') std::cout << "It's a tie!\n";
		else std::cout << "Player " << m_winner << " is the winner!\n";
	}
};